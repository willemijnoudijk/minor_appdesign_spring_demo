INSERT INTO genes (genbank_id, abbreviation, name, process) VALUES
  ('MN:123245', 'psi_OPS', 'Operator psi interaction', 'mental health'),
  ('AB"23412', 'FU-BAR', 'F**** up beyond all recognition', 'mental health'),
  ('NA:737373', 'prnETR', 'Goes to toilet often', 'metabolism'),
  ('NA:636254', 'dstrphy', 'Induces muscular dystrophy through inuse', 'physiology'),
  ('NA:755656', 'hngr', 'Hunger before lunch', 'metabolism');
