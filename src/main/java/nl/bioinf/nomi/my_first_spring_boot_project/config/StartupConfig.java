package nl.bioinf.nomi.my_first_spring_boot_project.config;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Created by michiel on 14/03/2017.
 */
@Component
public class StartupConfig {

    @EventListener
    public void handleContextRefresh(ContextRefreshedEvent event) {
        System.out.println("Context refreshed!");
    }
}
