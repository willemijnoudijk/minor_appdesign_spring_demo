package nl.bioinf.nomi.my_first_spring_boot_project.service;

import nl.bioinf.nomi.my_first_spring_boot_project.data_access.GeneDataSource;
import nl.bioinf.nomi.my_first_spring_boot_project.model.Gene;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by michiel on 02/03/2017.
 */
@Service
public class GeneService {

    private final GeneDataSource geneDataSource;

    @Autowired
    public GeneService(GeneDataSource geneDataSource) {
        this.geneDataSource = geneDataSource;
    }

    public Gene getGeneByAbbreviation(String abbreviation, boolean usePartMatching) {
        if (!usePartMatching) {
            return geneDataSource.getGeneByAbbreviation(abbreviation);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }
}
