package nl.bioinf.nomi.my_first_spring_boot_project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 * Created by michiel on 28/02/2017.
 */
@Service
@PropertySource("classpath:httpErrorCodes.properties")
public class ErrorService {

    @Autowired
    private Environment env;

    public String generateErrorMessage(final int errorCode) {
        String message = "There was a code " + errorCode + " error: ";
        switch (errorCode) {
            case 400:
                message += env.getProperty("400");
                break;
            case 401:
                message += env.getProperty("401");
                break;
            case 404:
                message += env.getProperty("404");
                break;
            case 500:
                message += env.getProperty("500");
                break;
            default:
                message += "I don't know this code - sorry";
                //etc
                //Put in all Http error codes here.
        }
        return message;
    }
}