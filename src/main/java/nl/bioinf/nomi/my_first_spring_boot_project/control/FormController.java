package nl.bioinf.nomi.my_first_spring_boot_project.control;

import nl.bioinf.nomi.my_first_spring_boot_project.model.FirstForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * Created by michiel on 27/02/2017.
 */
@Controller
public class FormController { // extends WebMvcConfigurerAdapter
//    @Override
//    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/result").setViewName("result");
//    }

    @GetMapping("/{locale}/submit_form")
    public String greetingForm(
            @PathVariable("locale") String locale,
            Model model) {
        model.addAttribute("firstForm", new FirstForm());
        return "first_form";
    }

    @PostMapping("/{locale}/submit_form")
    public String greetingSubmit(
            @PathVariable("locale") String locale,
            @Valid FirstForm firstForm,
            BindingResult bindingResult,
            RedirectAttributes redirect) {

        //if errors exist, go back to /submit_form
        if (bindingResult.hasErrors()) {
            return "first_form";
        }

        //if no errors exist, go to result page using redirect, to prevent form re-submission problems
        redirect.addFlashAttribute("firstForm", firstForm);
        return "redirect:/" + locale + "/result";
    }

    @RequestMapping("/{locale}/result")
    public String result(
            @PathVariable("locale") String locale,
            Model model) {
        return "result";
    }
}
