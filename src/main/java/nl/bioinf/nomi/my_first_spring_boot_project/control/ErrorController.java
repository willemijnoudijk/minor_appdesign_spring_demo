package nl.bioinf.nomi.my_first_spring_boot_project.control;

import nl.bioinf.nomi.my_first_spring_boot_project.service.ErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by michiel on 28/02/2017.
 */
@Controller
public class ErrorController {

    @Autowired
    private ErrorService errorService;

    @RequestMapping(value = "/errors")
    public String renderErrorPage(Model model, final HttpServletRequest request) {

        //Get the Http error code.
        final int errorCode = getHttpStatusCode(request);

        //Generates Error message for corresponding Http Error Code.
        final String errorMessage = errorService.generateErrorMessage(errorCode);
        model.addAttribute("errorMsg", errorMessage);

        //System.out.println("errorMessage = " + errorMessage);
        return "error";
    }

    private int getHttpStatusCode(final HttpServletRequest request) {
        return (int) request.getAttribute("javax.servlet.error.status_code");
    }
}
