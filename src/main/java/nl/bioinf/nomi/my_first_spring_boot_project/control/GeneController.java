package nl.bioinf.nomi.my_first_spring_boot_project.control;

import nl.bioinf.nomi.my_first_spring_boot_project.model.Gene;
import nl.bioinf.nomi.my_first_spring_boot_project.service.GeneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by michiel on 02/03/2017.
 */
@Controller
public class GeneController {

    private final GeneService geneService;

    @Autowired
    public GeneController(GeneService geneService) {
        this.geneService = geneService;
    }

    @RequestMapping("/{locale}/gene/{abbreviation}")
    public String geneByAbbreviation(
            @PathVariable("locale") String locale,
            @PathVariable("abbreviation") String abbreviation,
            Model model) {
        Gene example = geneService.getGeneByAbbreviation(abbreviation, false);
        model.addAttribute("gene", example);
        return "gene";
    }
}
