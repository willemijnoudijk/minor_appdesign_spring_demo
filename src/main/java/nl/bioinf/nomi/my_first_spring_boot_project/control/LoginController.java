package nl.bioinf.nomi.my_first_spring_boot_project.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Locale;

/**
 * Created by michiel on 22/03/2017.
 */
@Controller
public class LoginController {

    @RequestMapping(value="login")
    public String doLoginWithoutLocale(Locale locale) {
        return "/login";
    }

}
