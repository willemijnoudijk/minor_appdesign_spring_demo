package nl.bioinf.nomi.my_first_spring_boot_project.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by michiel on 22/03/2017.
 */
public class User {
    private static User guestUser;
    @NotNull
    @Size(min = 3, max = 30)
    private String username;
    @NotNull
    @Size(min = 3, max = 20)
    private String password;

    static {
        guestUser = new User();
        guestUser.setUsername("Guest");
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static User getGuest() {
        return guestUser;
    }
}
