package nl.bioinf.nomi.my_first_spring_boot_project.data_access.jdbc;

import nl.bioinf.nomi.my_first_spring_boot_project.data_access.GeneDataSource;
import nl.bioinf.nomi.my_first_spring_boot_project.model.Gene;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by michiel on 02/03/2017.
 */
@Component
@Profile("prod")
public class GeneDataSourceJdbc implements GeneDataSource {

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;


    @Autowired
    public GeneDataSourceJdbc(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    @Override
    public Gene getGeneById(String id) {
        return null;
    }

    @Override
    public Gene getGeneByName(String namePattern) {
        return null;
    }

    @Override
    public Gene getGeneByAbbreviation(String abbreviation) {
        return namedJdbcTemplate.queryForObject("SELECT * FROM genes WHERE abbreviation =:abbr",
                new MapSqlParameterSource("abbr", abbreviation),
                BeanPropertyRowMapper.newInstance(Gene.class));
    }
}
