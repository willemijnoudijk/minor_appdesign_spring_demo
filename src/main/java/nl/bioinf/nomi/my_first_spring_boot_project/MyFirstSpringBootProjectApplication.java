package nl.bioinf.nomi.my_first_spring_boot_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;

@SpringBootApplication
public class MyFirstSpringBootProjectApplication {
    private static final String PATH = "/errors";

    public static void main(String[] args) {
        SpringApplication.run(MyFirstSpringBootProjectApplication.class, args);
    }

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {

        return (container -> {
            ErrorPage error401Page = new ErrorPage(HttpStatus.UNAUTHORIZED, PATH);
            ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, PATH);
            ErrorPage error500Page = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, PATH);

            container.addErrorPages(error401Page, error404Page, error500Page);
        });
    }
}
